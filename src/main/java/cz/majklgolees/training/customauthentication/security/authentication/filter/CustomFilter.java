package cz.majklgolees.training.customauthentication.security.authentication.filter;

import cz.majklgolees.training.customauthentication.security.authentication.CustomAuthentication;
import cz.majklgolees.training.customauthentication.security.authentication.manager.CustomManager;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@AllArgsConstructor
public class CustomFilter extends OncePerRequestFilter {
    private final String key;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        var authKey = request.getHeader("x-auth-key");
        if(authKey == null) {
            filterChain.doFilter(request, response);
            return;
        }
        var customManager = new CustomManager(key);
        var auth = customManager.authenticate(new CustomAuthentication(false, authKey));
        if(auth.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(auth);
            filterChain.doFilter(request, response);
        }
    }
}
