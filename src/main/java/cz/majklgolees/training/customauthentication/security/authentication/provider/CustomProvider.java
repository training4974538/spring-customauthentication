package cz.majklgolees.training.customauthentication.security.authentication.provider;

import cz.majklgolees.training.customauthentication.security.authentication.CustomAuthentication;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@AllArgsConstructor
public class CustomProvider implements AuthenticationProvider {
    private String key;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if(supports(authentication.getClass())) {
            var customAuthentication = (CustomAuthentication) authentication;
            if(customAuthentication.getKey().equals(key)) {
                return new CustomAuthentication(true, null);
            }
            throw new BadCredentialsException("Bad credentials!");
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(CustomAuthentication.class);
    }
}
