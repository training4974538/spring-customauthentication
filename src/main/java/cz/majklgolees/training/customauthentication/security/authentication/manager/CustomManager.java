package cz.majklgolees.training.customauthentication.security.authentication.manager;

import cz.majklgolees.training.customauthentication.security.authentication.provider.CustomProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@AllArgsConstructor
public class CustomManager implements AuthenticationManager {
    private String key;
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        var customProvider = new CustomProvider(key);
        if(customProvider.supports(authentication.getClass())) {
            return customProvider.authenticate(authentication);
        }
        return authentication;
    }
}
